# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This contains the detailed explanation of implementation of an English language spell checker developed using Haskell functional programming language. 

Spell Checker can be used to spell check either a manually entered text or text loaded from a text file. This uses two Haskell implementations of suggestions generation algorithms known as SOUNDEX phonetic algorithm and Edit Distance algorithm. Most probable correction is selected by combining these two algorithms which is used for implementing an auto-correcting module to the spell checker. Program is tested using black box and white box testing to further clarify its performance. Refer to section-1 for further details.


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact